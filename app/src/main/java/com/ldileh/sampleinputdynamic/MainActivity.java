package com.ldileh.sampleinputdynamic;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();

    public List<ModelData> mItems;
    private ListAdapter adapter;

    private TextView tv_text;
    private Button btn_add;
    private ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init variable
        mItems = new ArrayList<>();

        // init view
        list = (ListView) findViewById(R.id.list);

        // configure listview
        LayoutInflater inflater = getLayoutInflater();
        View footer = inflater.inflate(R.layout.footer_list, null);
        btn_add = (Button) findViewById(R.id.btn_add);
        Button btn_check = (Button) findViewById(R.id.btn_check);
        tv_text = (TextView) footer.findViewById(R.id.text);
        list.addFooterView(footer);

        // set adapter listiview
        adapter = new ListAdapter(MainActivity.this);
        list.setAdapter(adapter);

        // event
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewInput();

                // desable button add
                btn_add.setEnabled(false);

                // force to focus end of item
                list.setSelection(adapter.getCount() - 1);
            }
        });

        btn_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOutPut(tv_text);
            }
        });
    }

    private void showOutPut(TextView textView){
        String text = "";
        String separator = ", ";

        for (int i = 0; i < mItems.size(); i++) {
            if((i + 1) == mItems.size()){
                separator = "";
            }

            text += mItems.get(i).getText() + separator;
        }

        if(text.isEmpty()){
            text = "...";

            // enable add
            btn_add.setEnabled(true);
        }

        textView.setText(text);
    }

    private void createNewInput(){
        // create new Item
        String id_input = Integer.toString(mItems.size() + 1);
        ModelData data = new ModelData();
        data.setId(id_input);
        data.setText("");
        mItems.add(data);

        // update list view
        adapter.notifyDataSetChanged();
    }

    private boolean checkListItem(){
        boolean result = true;

        for (ModelData items : mItems){
            if(items.getText().isEmpty()){
                result = false;
                break;
            }
        }

        return result;
    }

    private class ListAdapter extends BaseAdapter{

        private Context context;

        public ListAdapter(Context context){
            this.context = context;
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if(convertView == null){
                holder = new ViewHolder();

                LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.list_item, null);

                holder.edt_text = (EditText) convertView.findViewById(R.id.edt_text);
                holder.btn_close = (Button) convertView.findViewById(R.id.btn_close);
                holder.btn_update = (Button) convertView.findViewById(R.id.btn_update);

                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }

            // chek list item value
            String text = mItems.get(position).getText();
            holder.edt_text.setText(text);

            // fix bugs when button update appears when lost state
            holder.btn_update.setVisibility(View.GONE);

            // set focus on last item
            if((position + 1) == mItems.size())
                holder.edt_text.requestFocus();

            // set event
            holder.edt_text.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // show button
                    String text = s.toString();

                    if(!text.isEmpty())
                        holder.btn_update.setVisibility(View.VISIBLE);
                    else
                        holder.btn_update.setVisibility(View.GONE);

                    // check if in list item is empty
                    if(!checkListItem()){
                        btn_add.setEnabled(false);
                    }
                }
            });

            holder.btn_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // remove list item
                    mItems.remove(position);

                    // update list
                    notifyDataSetChanged();

                    // update output text
                    showOutPut(tv_text);
                }
            });

            holder.btn_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String text = holder.edt_text.getText().toString();
                    mItems.get(position).setText(text);

                    notifyDataSetChanged();

                    // enable button add on main view
                    btn_add.setEnabled(true);

                    // hide button update
                    holder.btn_update.setVisibility(View.GONE);
                }
            });

            return convertView;
        }

        private class ViewHolder{
            EditText edt_text;
            Button btn_close, btn_update;
        }
    }

    private class ModelData{
        String id;
        String text = "";

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
